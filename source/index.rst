.. Translation of Number Systems documentation master file, created by
   sphinx-quickstart on Thu Apr 29 17:15:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Translation of Number Systems's documentation!
=========================================================

.. toctree::
   :maxdepth: 2

   main
   functions
   converter

